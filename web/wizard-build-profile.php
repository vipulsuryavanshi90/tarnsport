
<?php 

echo "hello".'<br>';
echo "hello".'<br>';

echo "hello".'<br>';

echo "hello".'<br>';

echo "hello".'<br>';

echo "hello".'<br>';

include("../api/customer/generate_inquiry_id.php");
  echo "hello".$_SESSION['temp_inquiry_id'];
 ?>
<!doctype html>
<html lang="en" ng-app = "transpoterApp">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/favicon.ico">

	<title>Material Bootstrap Wizard by Creative Tim</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png" />
	<link rel="icon" type="image/png" href="assets/img/favicon.png" />

	<!--     Fonts and icons     -->
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

	<!-- CSS Files -->
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
	<link href="assets/css/web.css" rel="stylesheet" />
</head>

<body ng-controller="addInqueryController">

	<div class="image-container set-full-height" style="background-image: url('assets/img/wizard-profile.jpg')">
	<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Logo</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav lishadow">
        <li class="active"><a href="#">Home</a></li>
        <li><a href="#">Post Inquiry</a></li>
        <li><a href="#">Ongoing Inquiry</a></li>
        <li><a href="#">Completed Inquiry</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right lishadow">
        <li><a href="#"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
      </ul>
    </div>
  </div>
</nav>
	    <!--   Creative Tim Branding   -->
	  

		<!--  Made With Material Kit  -->
		<a href="http://demos.creative-tim.com/material-kit/index.html?ref=material-bootstrap-wizard" class="made-with-mk">
			<div class="brand">MK</div>
			<div class="made-with">Made with <strong>Material Kit</strong></div>
		</a>

	    <!--   Big container   -->
  <div class="container">
    <div class="row">
      <div class="col-sm-8 ">
        <!--      Wizard container        -->
        <div class="wizard-container" style="padding-top:50px;">
          <div class="card wizard-card" data-color="green" id="wizardProfile">
            <form action="" method="">
            	<div class="wizard-header">
                	<h3 class="wizard-title">
                	   Build Your Profile
                	</h3>
									<h5>This information will let us know more about you.</h5>
            	</div>
							<div class="wizard-navigation ">
								<ul>
                  <li><a href="#about" data-toggle="tab">Post Inquiry</a></li>
                  <li><a href="#account" data-toggle="tab">Adress Details</a></li>
                  <li><a href="#address" data-toggle="tab">Review And Post Inquiry</a></li>
                </ul>
							</div>
              <div class="tab-content">
                <div class="tab-pane" id="about">
                	<div class="well">
                  		<div class="row">
			    							<div class="panel panel-success">
			        						<div class="panel-heading panelhead">
			            					<h3 class="panel-title">Item Details</h3>
			        						</div>
									        <div class="panel-body">
									        	<div class="col-sm-6">
															<div class="input-group">
																<span class="input-group-addon">
																	<i></i>
																</span>
																<div class="form-group label-floating">
	                                <label class="control-label">Item Name (required)</label>
	                                <input name="itemname" type="text" class="form-control" ng-model="data.itemName">
										            </div>
															</div>
														</div>
														<div class="col-sm-4 col-sm-offset-1">
							                <div class="input-group">
																<div class="form-group label-floating">
                                  <label class="control-label">Quantity(required)</label>
                                  <input name="itemquantity" type="text" class="form-control" ng-model="data.itemQuantity">
                                </div>
															</div>
							              </div>
							              <div class="col-sm-10">
															<div class="input-group">
																<span class="input-group-addon">
																	<i></i>
																</span>
																<div class="form-group label-floating">
	                                <label class="control-label">Description(required)</label>
	                                <textarea name="itemdescription" class="form-control" ng-model="data.itemDescription"></textarea>
	                            	</div>
															</div>
							              </div>
									        </div>
									        <div class="panel-heading panelhead">
										        <h3 class="panel-title">Item Dimensions</h3>
										      </div>
										      <div class="panel-body">
										        <div class="row">
											        <div class="col-sm-4 col-sm-offset-4">
																<div class="input-group">
																	<span class="input-group-addon">
																		<i></i>
																	</span>
																	<div class="form-group label-floating">
	                                	<label class="control-label">Unit (required)</label>
	                                	<select name="itemdimensions" type="text" class="form-control" ng-model="data.itemdimensions">
	                                		<option>Meter</option>
	                                		<option>Feet</option>
	                                	</select>
								                  </div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-sm-4">
																<div class="input-group">
																	<span class="input-group-addon">
																		<i></i>
																	</span>
																	<div class="form-group label-floating">
                                    <label class="control-label">Length(required)</label>
                                    <input name="itemlength" type="text" class="form-control" ng-model="data.itemLength">
								                  </div>
																</div>
															</div>
															<div class="col-sm-4">
																<div class="input-group">
																	<span class="input-group-addon">
																		<i></i>
																	</span>
																	<div class="form-group label-floating">
                                    <label class="control-label">Width(required)</label>
                                    <input name="itemwidth" type="text" class="form-control" ng-model="data.itemWidth">
                                  </div>
																</div>
															</div>
															<div class="col-sm-4">
																<div class="input-group">
																	<span class="input-group-addon">
																		<i></i>
																	</span>
																	<div class="form-group label-floating">
                                    <label class="control-label">Height(required)</label>
                                    <input name="itemheigth" type="text" class="form-control" ng-model="data.itemHeigth">
                                  </div>
																</div>
															</div>
														</div>
														<div class="row">
												      <div class="col-sm-4">
																<div class="input-group">
																	<span class="input-group-addon">
																		<i></i>
																	</span>
																	<div class="form-group label-floating">
	                                	<label class="control-label">Unit (required)</label>
	                                	<select name="itemewightunit" type="text" class="form-control" ng-model="data.itemWeightUnit">
	                                		<option>Tonnes</option>
	                                		<option>Kilograms</option>
	                                	</select>
	                              	</div>
																</div>
															</div>
															<div class="col-sm-4">
																<div class="input-group">
																	<span class="input-group-addon">
																		<i></i>
																	</span>
																	<div class="form-group label-floating">
                                    <label class="control-label">Weight(required)</label>
                                    <input name="itemweight" type="text" class="form-control" ng-model="data.itemWeight">
                                  </div>
																</div>
															</div>
														</div>
										      </div>
										      <div class="panel-heading panelhead">
										        <h3 class="panel-title">Vehicle Details</h3>
										      </div>
										      <div class="panel-body">
										      	<div class="row">
											       	<div class="col-sm-4">
																<div class="input-group">
																	<span class="input-group-addon">
																		<i></i>
																	</span>
																	<div class="form-group label-floating">
	                                	<label class="control-label">Capacity (required)</label>
	                                	<select name="truckcapacity" type="text" class="form-control" ng-model="data.truckCapacity">
	                                		<option>5 Tonne</option>
	                                		<option>6 Tonne</option>
	                                	</select>
	                                </div>
																</div>
															</div>
															<div class="col-sm-4">
																<div class="input-group">
																	<span class="input-group-addon">
																		<i></i>
																	</span>
																	<div class="form-group label-floating">
                                  	<label class="control-label">Length (required)</label>
                                  	<select name="trucklength" type="text" class="form-control" ng-model="data.truckLength">
                                  		<option>5 Meter</option>
                                  		<option>6 Meter</option>
                                  	</select>
                                  </div>
																</div>
															</div>
															<div class="col-sm-4">
																<div class="input-group">
																	<span class="input-group-addon">
																		<i></i>
																	</span>
																	<div class="form-group label-floating">
                                  	<label class="control-label">Unit (required)</label>
                                  	<select name="trucktype" type="text" class="form-control"
                                  	ng-model="data.truckType">
                                  		<option>Truck</option>
                                  		<option>Tata Ace</option>
                                  	</select>
                                	</div>
																</div>
															</div>
														</div>
										      </div>
			    							</div>
											</div>
										</div>
										<div class="wizard-footer">
											<div class="pull-left">
	                        <input type='button' class='btn  btn-fill btn-success btn-wd' name='next' ng-click = "addItemDetails()" value='Add More'  />
	                    </div>
	                    <div class="pull-right">
	                        <input type='button' class='btn btn-next btn-fill btn-success btn-wd' name='next' ng-click = "addItemDetails()" value='Next' />
	                    </div>
                    	<div class="clearfix"></div>
                		</div>
									</div>
								
								
                  <div class="tab-pane" id="account">
                      
                      <div class="row">
                        <div class="col-sm-6 well">
                          <div class="col-sm-12">
                            <h4 class="info-text"> FROM </h4>
                          </div>
                          <div class="col-sm-12">
                            <div class="form-group label-floating">
                            	<label class="control-label">Address</label>
                        			<input type="text" name="formAddress" class="form-control" ng-model="address.from_address">
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group label-floating">
                              <label class="control-label">State</label>
                              <input type="text" name="formState" class="form-control" ng-model="address.from_state">
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">City</label>
                                <input type="text" name="formCity" class="form-control" ng-model="address.from_city">
                            </div>
                          </div>
                          <div class="col-sm-6 ">
                            <div class="form-group label-floating">
                                <label class="control-label">Pincode</label>
                                <input type="text" name="formPincode" class="form-control" ng-model="address.from_pincode">
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-6 well">
                          <div class="col-sm-12">
                            <h4 class="info-text"> To</h4>
                          </div>
                          <div class="col-sm-12">
                            <div class="form-group label-floating">
                            	<label class="control-label">Address</label>
                        			<input type="text" name="toAddress" class="form-control" ng-model="address.to_address">
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group label-floating">
                              <label class="control-label">State</label>
                              <input type="text" name="toState" class="form-control" ng-model="address.to_state">
                            </div>
                          </div>
                          <div class="col-sm-6 ">
                            <div class="form-group label-floating">
                                <label class="control-label">City</label>
                                <input type="text" name="toCity" class="form-control" ng-model="address.to_city">
                            </div>
                          </div>
                          <div class="col-sm-6 ">
                            <div class="form-group label-floating">
                                <label class="control-label">Pincode</label>
                                <input type="text" name="toPincode" class="form-control" ng-model="address.to_pincode">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="wizard-footer">
	                    <div class="pull-right">
	                        <input type='button' class='btn btn-next btn-fill btn-success btn-wd' name='next' ng-click = "addAddressDetails()" value='Next(address)' />
	                    </div>
	                    <div class="pull-left">
	                        <input type='button' class='btn btn-previous btn-fill btn-default btn-wd' name='previous' value='Previous' />
	                    </div>
                    	<div class="clearfix"></div>
                		</div>
                  </div>
                  <div class="tab-pane" id="address">
                  	<div class="well">
	                  	<div class="row">
	                  		<div class="panel panel-success">
			        						<div class="panel-heading panelhead">
			            					<h3 class="panel-title">Address</h3>
			        						</div>
									        <div class="panel-body">
		                        <div class="col-sm-6 well">
		                          <div class="col-sm-12">
		                            <h4 class="info-text"> FROM </h4>
		                          </div>
		                          <div class="col-sm-12">
		                            <div class="form-group">
		                            	<label class="control-label">Address</label>
		                        			<input type="text" name="formAddress" class="form-control" ng-model="address.from_address" readonly>
		                            </div>
		                          </div>
		                          <div class="col-sm-6">
		                            <div class="form-group">
		                              <label class="control-label">State</label>
		                              <input type="text" name="formState" class="form-control" ng-model="address.from_state" readonly>
		                            </div>
		                          </div>
		                          <div class="col-sm-6">
		                            <div class="form-group">
		                                <label class="control-label">City</label>
		                                <input type="text" name="formCity" class="form-control" ng-model="address.from_city" readonly>
		                            </div>
		                          </div>
		                          <div class="col-sm-6 ">
		                            <div class="form-group">
		                                <label class="control-label">Pincode</label>
		                                <input type="text" name="formPincode" class="form-control" ng-model="address.from_pincode" readonly>
		                            </div>
		                          </div>
		                        </div>
		                        <div class="col-sm-6 well">
		                          <div class="col-sm-12">
		                            <h4 class="info-text"> To</h4>
		                          </div>
		                          <div class="col-sm-12">
		                            <div class="form-group">
		                            	<label class="control-label">Address</label>
		                        			<input type="text" name="toAddress" class="form-control" ng-model="address.to_address" readonly>
		                            </div>
		                          </div>
		                          <div class="col-sm-6">
		                            <div class="form-group">
		                              <label class="control-label">State</label>
		                              <input type="text" name="toState" class="form-control" ng-model="address.to_state" readonly>
		                            </div>
		                          </div>
		                          <div class="col-sm-6 ">
		                            <div class="form-group">
		                                <label class="control-label">City</label>
		                                <input type="text" name="toCity" class="form-control" ng-model="address.to_city" readonly >
		                            </div>
		                          </div>
		                          <div class="col-sm-6 ">
		                            <div class="form-group">
		                                <label class="control-label">Pincode</label>
		                                <input type="text" name="toPincode" class="form-control" ng-model="address.to_pincode" readonly>
		                            </div>
		                          </div>
			                      </div>
			                    </div><!-- panel body-->
	                      </div> <!-- panel -->
	                      <div class="panel panel-success">
	                      	<div class="panel-heading panelhead">
	                      		<h3 class="panel-title">Shipment Details</h3>
	                      	</div>
	                      	<div class="panel-body">
														<div class="panel-group" id="accordion">
			                      	<div class="panel panel-default" ng-repeat="item in itemdetails">

				                      	<a data-toggle="collapse" data-parent="#accordion" href="#{{item.item_id}}">
					                      	<div class="panel-heading" style="background-color:#f5f5f5">
					                      		<h5 class="panel-title">Shipment Details</h5>
					                      	</div> 
					                      </a>
						                    <div id="{{item.item_id}}" class="panel-collapse collapse">
				                      		<div class="panel-body" id="demo" class="collapse">
					                      		<div class="row">
						                      		<div class="col-sm-6">
																				<div class="input-group">
																					<span class="input-group-addon">
																						<i></i>
																					</span>
																					<div class="form-group label-floating">
						                                <label class="control-label">Item Name (required)</label>
						                                <input name="itemname" type="text" class="form-control"  ng-model="item.item_name">
															            </div>
																				</div>
																			</div>
																			<div class="col-sm-4 col-sm-offset-1">
												                <div class="input-group">
																					<div class="form-group label-floating">
					                                  <label class="control-label">Quantity(required)</label>
					                                  <input name="itemquantity" type="text" class="form-control" ng-model="item.item_quantity">
					                                </div>
																				</div>
												              </div>
												              <div class="col-sm-10">
																				<div class="input-group">
																					<span class="input-group-addon">
																						<i></i>
																					</span>
																					<div class="form-group label-floating">
						                                <label class="control-label">Description(required)</label>
						                                <textarea name="itemdescription" class="form-control" ng-model="item.item_desc"></textarea>
						                            	</div>
																				</div>
												              </div>
											              </div> <!-- row -->
					                      		<div class="row">
															        <div class="col-sm-4">
																				<div class="input-group">
																					<span class="input-group-addon">
																						<i></i>
																					</span>
																					<div class="form-group label-floating">
					                                	<label class="control-label">Unit (required)</label>
					                                	<select name="itemdimensions" type="text" class="form-control" ng-model="itemdimensions">
					                                		<option>Meter</option>
					                                		<option>Feet</option>
					                                	</select>
												                  </div>
																				</div>
																			</div>
																		</div>
																		<div class="row">
																			<div class="col-sm-4">
																				<div class="input-group">
																					<span class="input-group-addon">
																						<i></i>
																					</span>
																					<div class="form-group label-floating">
				                                    <label class="control-label">Length(required)</label>
				                                    <input name="itemlength" type="text" class="form-control" ng-model="item.item_length">
												                  </div>
																				</div>
																			</div>
																			<div class="col-sm-4">
																				<div class="input-group">
																					<span class="input-group-addon">
																						<i></i>
																					</span>
																					<div class="form-group label-floating">
				                                    <label class="control-label">Width(required)</label>
				                                    <input name="itemwidth" type="text" class="form-control" ng-model="item.item_width">
				                                  </div>
																				</div>
																			</div>
																			<div class="col-sm-4">
																				<div class="input-group">
																					<span class="input-group-addon">
																						<i></i>
																					</span>
																					<div class="form-group label-floating">
				                                    <label class="control-label">Height(required)</label>
				                                    <input name="itemheigth" type="text" class="form-control" ng-model="item.item_height">
				                                  </div>
																				</div>
																			</div>
																		</div>
																		<div class="row">
																      <div class="col-sm-4">
																				<div class="input-group">
																					<span class="input-group-addon">
																						<i></i>
																					</span>
																					<div class="form-group label-floating">
					                                	<label class="control-label">Unit (required)</label>
					                                	<select name="itemewightunit" type="text" class="form-control" ng-model="itemweightunit">
					                                		<option>Tonnes</option>
					                                		<option>Kilograms</option>
					                                	</select>
					                              	</div>
																				</div>
																			</div>
																			<div class="col-sm-4">
																				<div class="input-group">
																					<span class="input-group-addon">
																						<i></i>
																					</span>
																					<div class="form-group label-floating">
				                                    <label class="control-label">Weight(required)</label>
				                                    <input name="itemweight" type="text" class="form-control" ng-model="item.item_weight">
				                                  </div>
																				</div>
																			</div>
																		</div> <!-- row -->
																		<div class="row">
																			<div class="col-sm-4">
																				<div class="input-group">
																					<span class="input-group-addon">
																						<i></i>
																					</span>
																					<div class="form-group label-floating">
				                                    <label class="control-label">Vehicle Capacity(required)</label>
				                                    <input name="itemweight" type="text" class="form-control" ng-model="item.truck_capacity">
				                                  </div>
																				</div>
																			</div>
																			<div class="col-sm-4">
																				<div class="input-group">
																					<span class="input-group-addon">
																						<i></i>
																					</span>
																					<div class="form-group label-floating">
				                                    <label class="control-label">Vehicle Length(required)</label>
				                                    <input name="itemweight" type="text" class="form-control" ng-model="item.truck_length">
				                                  </div>
																				</div>
																			</div>
																			<div class="col-sm-4">
																				<div class="input-group">
																					<span class="input-group-addon">
																						<i></i>
																					</span>
																					<div class="form-group label-floating">
				                                    <label class="control-label">Vehicle Type(required)</label>
				                                    <input name="itemweight" type="text" class="form-control" ng-model="item.truck_type">
				                                  </div>
																				</div>
																			</div>
																		</div> <!-- row -->
			                      			</div> <!-- panel body -->
			                      			<div>
				                      			<div class="panel-footer">
				                      				<div class="pull-right">
				                      					<input type='button' class='btn btn-warning' name='Update' value='Save Changes' ng-click='update_item(item.item_id)'/>
				                      					<!-- <div class="clearfix"></div> -->
				                      				</div>
				                      			</div>
			                      			</div>
			                      		</div> <!-- collapse in -->
		                      		</div><!-- panel -->
		                      	</div> <!-- panel-group(accordian) -->
		                      </div> <!-- panel-body -->
		                    </div> <!-- panel -->
	                      <div class="wizard-footer">
			                    <div class="pull-right">
			                        <input type='button' class='btn btn-next btn-fill btn-success btn-wd' name='next' value='Next' />
			                        <input type='button' class='btn btn-finish btn-fill btn-success btn-wd' name='Post Inquery' value='Post Inquery'/>
			                    </div>

			                    <div class="pull-left">
			                        <input type='button' class='btn btn-previous btn-fill btn-default btn-wd' name='previous' value='Previous' />
			                    </div>
			                    <div class="clearfix"></div>
			                	</div>
	                    </div> <!-- row -->
	                  </div> <!-- well -->
                      <div class="row">
                          <div class="col-sm-12">
                              <h4 class="info-text"> ? </h4>
                          </div>
                          <div class="col-sm-7 col-sm-offset-1">
                            	<div class="form-group label-floating">
                            		<label class="control-label">Street Name</label>
                        			<input type="text" class="form-control">
                            	</div>
                          </div>
                          <div class="col-sm-3">
                              <div class="form-group label-floating">
                                  <label class="control-label">Street Number</label>
                                  <input type="text" class="form-control">
                              </div>
                          </div>
                          <div class="col-sm-5 col-sm-offset-1">
                              <div class="form-group label-floating">
                                  <label class="control-label">City</label>
                                  <input type="text" class="form-control">
                              </div>
                          </div>
                          <div class="col-sm-5">
                              <div class="form-group label-floating">
                                  <label class="control-label">Country</label>
                                	<select name="country" class="form-control">
																			<option disabled="" selected=""></option>
                                    	<option value="Afghanistan"> Afghanistan </option>
                                    	<option value="Albania"> Albania </option>
                                    	<option value="Algeria"> Algeria </option>
                                    	<option value="American Samoa"> American Samoa </option>
                                    	<option value="Andorra"> Andorra </option>
                                    	<option value="Angola"> Angola </option>
                                    	<option value="Anguilla"> Anguilla </option>
                                    	<option value="Antarctica"> Antarctica </option>
                                    	<option value="...">...</option>
                                	</select>
                              </div>
                          </div>
                      </div>
                      
                  </div>
              </div>
                
            </form>
          </div>
        </div> <!-- wizard container -->
      </div>
       <div class="col-sm-4">
        <div class="wizard-container" style="padding-top:50px;">
        	<div class="card">
        		<div class="col-sm-4 col-sm-offset-1">
                                  	<div class="picture-container">
                                      	<div class="picture">
                                  				<img src="assets/img/default-avatar.png" class="picture-src" id="wizardPicturePreview" title=""/>
                                          	<input type="file" id="wizard-picture">
                                      	</div>
                                      	<h6>Choose Picture</h6>
                                  	</div>
                              	</div>
        	</div>
       
        </div>
      </div>
     
    </div><!-- end row -->
  </div> <!--  big container -->

	    <div class="footer">
	        <div class="container text-center">
	             Made with <i class="fa fa-heart heart"></i> by <a href="http://www.creative-tim.com">Creative Tim</a>. Free download <a href="http://www.creative-tim.com/product/bootstrap-wizard">here.</a>
	        </div>
	    </div>
	</div>

</body>
	<!--  Angularjs files-->
	<script src="assets/js/angular.js" type="text/javascript"></script>
  <script src="assets/js/transporter_angular.js" type="text/javascript"></script>
	<!--   Core JS Files   -->
  <script src="assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/js/jquery.bootstrap.js" type="text/javascript"></script>

	<!--  Plugin for the Wizard -->
	<script src="assets/js/material-bootstrap-wizard.js"></script>

    <!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
	<script src="assets/js/jquery.validate.min.js"></script>

</html>
